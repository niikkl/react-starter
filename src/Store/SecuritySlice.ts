import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface SecurityState {
    token: null | string;
}

const SecuritySlice = createSlice({
    name: 'security',
    initialState: {
        token: null
    } as SecurityState,
    reducers: {
        setToken: (state: SecurityState, action: PayloadAction<null | string>) => {
            state.token = action.payload;
        }
    }
});

export const {setToken} = SecuritySlice.actions;
export default SecuritySlice.reducer;