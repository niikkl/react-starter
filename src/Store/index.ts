import {combineReducers, configureStore} from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import {persistReducer, persistStore} from "redux-persist";
import storage from 'redux-persist/lib/storage';

// slices
import SecuritySlice from "./SecuritySlice";

const persistConfig = {
    key: 'root',
    storage
}

const rootReducer = combineReducers({
    security: SecuritySlice
});

export const store = configureStore({
    reducer: persistReducer(persistConfig, rootReducer),
    devTools: true
});
export const persistor = persistStore(store);
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()