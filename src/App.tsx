import React, {FC, ReactElement} from 'react';
import {Pane} from "evergreen-ui";
import {Switch} from "react-router-dom";
import {LayoutRoute} from "./Components/LayoutRoute/LayoutRoute";
import {FormLayout} from "./Layouts/FormLayout/FormLayout";
import {HomePage} from "./Pages/HomePage/HomePage";

export const App: FC = (): ReactElement => {
  return (
    <Pane className="app bg-gray-100 text-gray-700 h-full">
      <Switch>
        <LayoutRoute path="/" layout={FormLayout} page={HomePage}/>
      </Switch>
    </Pane>
  );
}
