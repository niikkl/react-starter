import {FC, ReactElement} from "react";
import {ReactComponentLike} from 'prop-types';
import {Route} from "react-router-dom";

interface LayoutRouteProps {
    path: string;
    layout: ReactComponentLike;
    page: ReactComponentLike;
}

export const LayoutRoute: FC<LayoutRouteProps> = ({path, ...props}): ReactElement => {
    return (
        <Route exact path={path}>
            <props.layout>
                <props.page/>
            </props.layout>
        </Route>
    );
};