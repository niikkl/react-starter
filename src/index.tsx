import React from 'react';
import ReactDOM from 'react-dom';
import './Theme/app.scss';
import {App} from './App';
import {Provider} from "react-redux";
import {persistor, store} from "./Store";
import { PersistGate } from 'redux-persist/integration/react';
import {HashRouter as Router} from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <Router>
                    <App/>
                </Router>
            </PersistGate>
        </Provider>
        <ToastContainer/>
    </React.StrictMode>,
    document.getElementById('root')
);