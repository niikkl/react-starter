import {FC, ReactElement, useState} from "react";
import {useForm} from "react-hook-form";
import * as yup from 'yup';
import {yupResolver} from "@hookform/resolvers/yup";
import {InferType} from "yup";
import {Button, Link, TextInputField, toaster} from "evergreen-ui";
import {RootState, useAppDispatch} from "../../Store";
import {setToken} from "../../Store/SecuritySlice";
import {useSelector} from "react-redux";
import {toast} from "react-toastify";

interface FormInputs {
    token: string;
}

const schema = yup.object({
    token: yup.string().nullable()
});

export const HomePage: FC = ({children}): ReactElement => {
    const security = useSelector((state: RootState) => state.security);
    const dispatch = useAppDispatch();

    const {register, handleSubmit, watch} = useForm<InferType<typeof schema>>({
        resolver: yupResolver(schema),
        defaultValues: {
            token: security.token
        }
    });

    const [loading, setLoading] = useState<boolean>(false);
    const tokenWatcher = watch('token');

    function onSubmit(data: FormInputs) {
        setLoading(true);

        // simulate loading
        setTimeout(() => {
            dispatch(setToken(data.token));
            setLoading(false);
            toast.success('Token saved successfully.');
        }, 1000);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-10 text-center">
                <div className="text-2xl">Example Form</div>
                <small>Lorem ipsum dolor sit amet, consetetur sadipscing <Link href="#">elitr</Link>.</small>
            </div>

            <div className="mb-3">
                <TextInputField
                    label="Security token"
                    hint="Will be stored in localstorage."
                    placeholder="Example field"
                    {...register('token')}
                />
            </div>

            <div className="flex justify-end">
                <Button disabled={tokenWatcher === security.token} isLoading={loading} type="submit" appearance="primary">Save</Button>
            </div>
        </form>
    );
}