import {FC, ReactElement} from "react";

export const BlankLayout: FC = ({children}): ReactElement => {
    return <>{children}</>;
}