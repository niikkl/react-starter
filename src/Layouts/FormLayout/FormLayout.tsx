import {FC, ReactElement} from "react";
import {Pane} from "evergreen-ui";

export const FormLayout: FC = ({children}): ReactElement => {
    return (
        <Pane className="flex justify-center items-center h-full w-full px-3">
            <Pane className="bg-white rounded shadow-sm p-8 2xl:w-3/12 xl:w-4/12 lg:w-5/12 md:w-6/12 sm:w-8/12 w-full">
                {children}
            </Pane>
        </Pane>
    );
}